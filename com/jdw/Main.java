package com.jdw;

public class Main {

    public static void main(String[] args) {
        Part set1 = new Tires(254,"Michelin",465_897_123_456L,"255/40");
        Part exhaust1 = new ExhaustComponents(269,"AC Schintzer", 465_897_123_456L,true);
        Part exhaust2 = new ExhaustComponents(103,"FlowMaster",465_897_123_456L,false);

        System.out.println(set1);
        System.out.println("================");
        System.out.println(exhaust1);
        System.out.println("================");
        System.out.println(exhaust2);
    }
}
