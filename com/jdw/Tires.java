package com.jdw;

public class Tires extends Part {
    private String size;

    Tires(int id, String make, long serNumber, String size) {
        super(id, make, serNumber);
        this.size = size;
    }
    @Override
    public String toString() {
        return "Make: " + getMake() + ".\nSize: " + size + ".\nProd. ID: " + getId() +
                ".\nSerial number: " + getSerNumber();
    }
}
