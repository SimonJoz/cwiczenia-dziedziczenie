package com.jdw;

public class ExhaustComponents extends Part{
    private boolean exhaustEmissionStd;

    ExhaustComponents(int id, String make, long serNumber, boolean emissionStd) {
        super(id, make, serNumber);
        this.exhaustEmissionStd = emissionStd;
    }
    @Override
    public String toString() {
        String str = "";
        if(exhaustEmissionStd) str += "YES";
        if(!exhaustEmissionStd) str += "NO!";
        return "Make: " + getMake() + ".\nProd. ID: " + getId() +
                ".\nComplies with European emission standards: " + str +
                ".\nSerial number: " + getSerNumber();
    }
}
