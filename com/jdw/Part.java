package com.jdw;

class Part {
    private int id;
    private String make;
    private long serNumber;

    Part(int id, String make, long serNumber) {
        this.id = id;
        this.make = make;
        this.serNumber = serNumber;
    }

    int getId() {
        return id;
    }

    String getMake() {
        return make;
    }

    long getSerNumber() {
        return serNumber;
    }
}
